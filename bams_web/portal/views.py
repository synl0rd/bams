# -*- coding: utf-8 -*-
from django.core.cache import cache
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.views.generic.simple import direct_to_template
from django.shortcuts import get_object_or_404, render_to_response
from django.template import Context, Template, RequestContext
from django.core import serializers
from django.core import urlresolvers
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.contrib import auth
from django.conf import settings
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.utils.encoding import smart_str, smart_unicode
from django.contrib.auth.decorators import login_required
from django import forms

import simplejson
import pexpect
import os
import uuid
import pymongo
import urllib2
import re
import commands
import smtplib

from subprocess import Popen
from pymongo import DESCENDING, ASCENDING
from datetime import datetime, timedelta

bw_db = pymongo.Connection().bams.bw
anomaly_db = pymongo.Connection().bams.anomaly
settings_db = pymongo.Connection().bams.settings

ADMIN_GMAIL_USER = 'uptitenas@gmail.com'
ADMIN_GMAIL_PASS = '1515sukses'

def send_mail(message, to):
    try:
        email_acc = ADMIN_GMAIL_USER
        email_pwd = ADMIN_GMAIL_PASS
        smtpserver = smtplib.SMTP("smtp.gmail.com",587)
        print 'connecting to smtp.gmail.com:587'
        smtpserver.ehlo()
        smtpserver.starttls()
        smtpserver.ehlo
        print 'ehlo smtp.gmail.com:587'
        smtpserver.login(email_acc, email_pwd)
        print 'logged in to smtp.gmail.com:587'
        header = 'To:' + to + '\n' + 'From: BAMS <'+email_acc+'>\n' + 'Subject: ALERT!!\n'
        msg = header + '\n'+message+'\n\n'
        smtpserver.sendmail(email_acc, to, msg)
        print 'sending mail to: %s' % to
        smtpserver.close()
    except Exception, err:
        print 'failed send email: %s' % err
        return False
    else:
        print 'sending mail successful'
        return True

def send_sms(message, to):
#    os.system('python sms_sender.py "%s" %s' %(message,to))
    cur_path = os.path.abspath(os.path.dirname(__file__))
    Popen(['python',os.path.join(cur_path, 'sms_sender.py'),'"%s"' % message, to], close_fds=True)
    return True

def login_page(request):
    return render_to_response('login.html', locals(), context_instance=RequestContext(request))

def turnon(request):
   cur_path = os.path.abspath(os.path.dirname(__file__))
   if request.POST.get('sudo'):
       sudo_pass = request.POST.get('sudo')
       print sudo_pass
       x = os.system('echo %s | sudo -S python %s > /dev/null 2>&1 &' % (sudo_pass, os.path.join(cur_path, '../../bams.py')))
#       x = Popen(['echo', '%s' % sudo_pass, '|', 'sudo', '-S', 'python','%s' % os.path.join(cur_path, '../../bams.py')], close_fds=True)
       print x
#   os.system('python %s > /dev/null 2>&1 &' % os.path.join(cur_path, '../backup/backup.py'))
   return HttpResponse(True)    

def turnoff(request):
   cur_path = os.path.abspath(os.path.dirname(__file__))
   if request.POST.get('sudo'):
       sudo_pass = request.POST.get('sudo')
       print sudo_pass
       x = os.system('echo %s | sudo -S python %s > /dev/null 2>&1 &' % (sudo_pass, os.path.join(cur_path, '../../killer.py')))
#       x = Popen(['echo', '%s' % sudo_pass, '|', 'sudo', '-S', 'python','%s' % os.path.join(cur_path, '../../bams.py')], close_fds=True)
       print x
#   os.system('python %s > /dev/null 2>&1 &' % os.path.join(cur_path, '../backup/backup.py'))
   return HttpResponse(True)    

@login_required(redirect_field_name='not_found')
def port_chart(request):
    bandwidth = []
    if request.POST.get('port_request'):
        timestamp = datetime.now()
        realtime = True
        port_request = int(request.POST.get('port_request'))
        try:
            timestamp = datetime(int(request.POST.get('date').split('-')[0]), int(request.POST.get('date').split('-')[1]), int(request.POST.get('date').split('-')[2]))
        except Exception, err:
            print err
        else:
            print timestamp
            if timestamp != datetime.utcnow() + timedelta(hours=7):
                realtime = False
                print 'not realtime'

        for i in bw_db.find().sort('timestamp', direction=DESCENDING):
            if i['timestamp'].day == timestamp.day and i['timestamp'].month == timestamp.month and i['timestamp'].year == timestamp.year:
                try:
                    down = i['down%s' % port_request]
                except Exception:
                    down = 0
                try:
                    up = i['up%s' % port_request]
                except Exception:
                    up = 0
                bandwidth.append({'timestamp':i['timestamp'],'up':up, 'down':down})

        return render_to_response('ajax/chart.html', locals(), context_instance=RequestContext(request))

def main(request):
    main_page = True
    for i in ['admin']:
        try:
            data_user = User.objects.create_user(username=i, 
                                                             email='%s@gmail.com' % i,
                                                             password='1234')
            data_user.is_staff = True
            data_user.first_name = i
            data_user.last_name = i
            data_user.save()
        except Exception, err:
            print err
                
    if request.GET.get('not_found'):
        return HttpResponseRedirect('/')
        
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('login_page'))
    
    
    check_bams = commands.getoutput('ps -ax | grep bams.py')
            
    service_up = False
    if re.search('python .*bams.py', check_bams):
        service_up = True
            
    timestamp = datetime.now()
    realtime = True
    bandwidth = []
    checker = []
    bandwidth_log = []

    try:
        timestamp = datetime(int(request.GET.get('date').split('-')[0]), int(request.GET.get('date').split('-')[1]), int(request.GET.get('date').split('-')[2]))
    except Exception, err:
        print err
    else:
        print timestamp
        if timestamp != datetime.utcnow() + timedelta(hours=7):
            realtime = False
            print 'not realtime'
    
    for i in bw_db.find().sort('timestamp', direction=DESCENDING):
        if i['timestamp'].day == timestamp.day and i['timestamp'].month == timestamp.month and i['timestamp'].year == timestamp.year:
            high_down = [None,0]
            high_up = [None,0]
            for x in i.keys():
                if re.search('down[\d]+', x):
                    if i[x] > high_down[-1]:
                        high_down = [x,i[x]]
                if re.search('up[\d]+', x):
                    if i[x] > high_up[-1]:
                        high_up = [x,i[x]]
            port_down = high_down[0].replace('down','')
            high_down = [port_down, high_down[1]]
            port_up = high_up[0].replace('up','')
            high_up = [port_up, high_up[1]]
            
            if int(high_down[0]) > 1023 and int(high_up[0]) > 1023 and float(high_up[1]) > 20 or int(high_down[0]) > 1023 and int(high_up[0]) > 1023 and float(high_down[1]) > 20:
                i.update({'high_down': high_down, 'high_up':high_up, 'anomaly':True})
                if not anomaly_db.find_one({'timestamp':i['timestamp'], 'ip':i['ip'], 'port_up':int(high_up[0]), 'port_down':int(high_down[0]), 'kbs_down':float(high_down[1]), 'kbs_up':float(high_up[1])}):
                    anomaly_db.insert({'timestamp':i['timestamp'], 'ip':i['ip'], 'port_up':int(high_up[0]), 'port_down':int(high_down[0]), 'kbs_down':float(high_down[1]), 'kbs_up':float(high_up[1])})                
            else:
                i.update({'high_down': high_down, 'high_up':high_up, 'anomaly':False})
            if settings_db.find_one({'username':str(request.user)}):
                to = settings_db.find_one({'username':str(request.user)})['email']
                to_sms = settings_db.find_one({'username':str(request.user)})['hp']
                mail_sent = send_mail('Alert! Anomaly Network Package Detected', to)
                sms_sent = send_sms('Alert! Anomaly Network Package Detected', to_sms)


            if '%s-%s-%s' % (i['timestamp'].year,i['timestamp'].month,i['timestamp'].day) in checker:
                down_before = bandwidth[-1]['high_down'][1]
                up_before = bandwidth[-1]['high_up'][1]
                high_down = [port_down, float(high_down[1])+float(down_before)]
                high_up = [port_up, float(high_up[1])+float(up_before)]
                i.update({'high_down': high_down, 'high_up':high_up})
                bandwidth[-1] = i
#            else:
            bandwidth.append(i)
            bandwidth_log.append(i)
#            checker.append('%s-%s-%s' % (i['timestamp'].year,i['timestamp'].month,i['timestamp'].day))
            

    paginator = Paginator(bandwidth_log, 10)
    try:
        try:
            page = int(request.GET.get('page', '1'))
        except ValueError:
            page = 1

        try:
            current_page = paginator.page(page)
        except (EmptyPage, InvalidPage):
            current_page = paginator.page(paginator.num_pages)

        bandwidth_log = current_page.object_list
        pages = paginator.page_range
    except Exception, err:
        pass       
    if request.POST.get('ajax') == 'true':
        return render_to_response('table_real.html', locals(), context_instance=RequestContext(request))
    return render_to_response('chart.html', locals(), context_instance=RequestContext(request))

@login_required(redirect_field_name='not_found')
def portal(request):
    return HttpResponse()

@login_required(redirect_field_name='not_found')
def command(request):
    try:
        ip = request.POST.get('ip')
        urllib2.urlopen('http://%s:1212/reboot' % ip)
    except Exception, err:
        print err
    return HttpResponse(True)

@login_required(redirect_field_name='not_found')
def delete_deploy(request):
    if request.POST.get('key'):
        coll_deploy.remove({'key': request.POST.get('key')})
        return HttpResponse(True)
    return HttpResponse(False)

def flush(request):
    bw_db.remove()
    return HttpResponseRedirect(reverse('main'))

@login_required(redirect_field_name='not_found')
def info_detail(request):
    server = coll_log.find_one({'key': request.GET.get('server')})
    server_info = coll_log.find({'key': request.GET.get('server')})
    return render_to_response('info_detail.html', locals(), context_instance=RequestContext(request))

@login_required(redirect_field_name='not_found')
def deploying(request):
    try:
        sudo_password = request.POST.get('sudo')
        CUR_PATH = os.path.abspath(os.path.dirname(__file__))
        deploy_data = {'key': str(uuid.uuid1()),
                   'username':request.POST.get('ssh_username'),
                   'password':request.POST.get('ssh_password'),
                   'ip':request.POST.get('ip'),
                   'label': request.POST.get('label'),
                   'up': False,
                   'timestamp': '%s-%s-%s-%s-%s-%s-%s' % (datetime.utcnow().year,
                                                          datetime.utcnow().month,
                                                          datetime.utcnow().day, 
                                                          datetime.utcnow().hour, 
                                                          datetime.utcnow().minute,
                                                          datetime.utcnow().second,
                                                          datetime.utcnow().microsecond
                                                         )
                   }
        if not request.POST.get('label') or request.POST.get('label') == '':
            return HttpResponse('Nama Label Server Harus Diisi')
        deploy_data = simplejson.dumps(deploy_data)    
        open(os.path.join(CUR_PATH, 'deploying.json'),'w').write(deploy_data)
        session = pexpect.spawn('fab remote copy')
    except Exception, err:
        print err
    
    try:
        session.expect('.*No existing session.*', timeout=10)
    except pexpect.ExceptionPexpect:
        pass
    else:
        session = pexpect.spawn('sudo fab remote copy')
    
    try:
        session.expect('\[sudo\] password for .*:', timeout=5)
    except pexpect.ExceptionPexpect:
        pass
    else:
        print 'sending %s' %sudo_password
        session.sendline(sudo_password)
    try:
        session.expect('.*Sorry, try again.')
    except Exception, err:
        pass
    else:
        return HttpResponse('Gagal')

#    try:
#        session.expect('.* password for .*:', timeout=3)
#    except pexpect.ExceptionPexpect:
#        pass
#    else:
#        print 'sending %s' %request.POST.get('ssh_password')
#        session.sendline(request.POST.get('ssh_password'))
#    try:
#        session.interact()
#    except:
#        pass
    try:
        if coll_deploy.find_one({'ip':request.POST.get('ip')}):
            return HttpResponse('IP sudah pernah di deploy')
        coll_deploy.insert(simplejson.loads(deploy_data))
    except Exception, err:
        print err
                
    return HttpResponse('True')

def signin(request):
    if request.method == 'POST':
        try:
            getuser = auth.authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
            auth.login(request, getuser)
        except Exception, err:
            return HttpResponseRedirect('/?error')
        else:
            return HttpResponseRedirect(reverse('main'))
    return HttpResponseRedirect(reverse('main'))

@login_required(redirect_field_name='not_found')
def settings(request):
    settings_page = True
    if request.POST.get('save_settings'):
        data = {'username': str(request.user),'hp': request.POST.get('hp'), 'email':request.POST.get('email')}
        if not settings_db.find_one({'username':str(request.user)}):
            settings_db.insert(data)
            return HttpResponseRedirect(reverse('settings'))
        else:
            settings_db.update({'username':str(request.user)}, {'$set':data})
    lookup = settings_db.find_one({'username': str(request.user)})
    return render_to_response('settings.html', locals(), context_instance=RequestContext(request))

@login_required(redirect_field_name='not_found')
def report(request):
    report_page = True
    anomalies = anomaly_db.find().sort('timestamp', direction=DESCENDING)
    timestamp = datetime.now()

    try:
        timestamp = datetime(int(request.GET.get('date').split('-')[0]), int(request.GET.get('date').split('-')[1]), int(request.GET.get('date').split('-')[2]))
    except Exception, err:
        print err
    return render_to_response('report.html', locals(), context_instance=RequestContext(request))


@login_required(redirect_field_name='not_found')
def signout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('main'))
