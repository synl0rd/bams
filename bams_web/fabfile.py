from fabric.api import *
#from fabric.operations import local as lrun
from fabric.context_managers import settings

import os
import sys
import pymongo
import simplejson
import logging
logging.basicConfig( level=logging.INFO )

CUR_PATH = os.path.abspath(os.path.dirname(__file__))
lookup = open(os.path.join(CUR_PATH, 'portal/deploying.json'), 'r').read()
lookup = simplejson.loads(lookup)

username = lookup['username']
ip = lookup['ip']
password = lookup['password']
label = lookup['label']
current_directory = os.path.abspath(os.path.dirname(__file__))

def remote():
    env.hosts = ['%s@%s' %(username,ip)]
    env.password = password

def copy():
    try:
        run('mkdir -p /home/%s/Desktop/monitoring/devel' % username)
        put(os.path.join(current_directory, 'source/psutil-0.4.1.tar.gz'), '/home/%s/Desktop/monitoring' % username)
        put(os.path.join(current_directory, 'source/web.py-0.36.tar.gz'), '/home/%s/Desktop/monitoring' % username)
        put(os.path.join(current_directory, 'source/devel/*'), '/home/%s/Desktop/monitoring/devel/' % username)
        put(os.path.join(current_directory, 'source/monitor.py'), '/home/%s/Desktop/monitoring' % username)
        put(os.path.join(current_directory, 'source/rest_service.py'), '/home/%s/Desktop/monitoring' % username)
        with cd('/home/%s/Desktop/monitoring' % username):
            run('echo "%s" > label.txt' % label)
            run('tar xzvf psutil-0.4.1.tar.gz')
            run('tar xzvf web.py-0.36.tar.gz')
#            run('tar xzvf setuptools-0.6c11.tar.gz')
            with cd('devel'):
                sudo('cp -rf * /usr/include/python2.6/')
            with cd('web.py-0.36'):
                sudo('python setup.py install')
            with cd('psutil-0.4.1'):
                sudo('python setup.py install')

#            with cd('setuptools-0.6c11'):
#                sudo('python setup.py install')


            sudo('python monitor.py > /dev/null', pty=False)
#            sudo('python rc.py', pty=False)
#            sudo('echo "python /home/%s/Desktop/monitoring/monitor.py\nexit 0" >> /etc/rc.local' % username)

            with settings(warn_only=True):
                sudo('chmod 777 /home/%s/Desktop/monitoring/performance.db' % username)
                sudo('chmod 777 /tmp/monitor.pid')
            sudo('''echo "#!/usr/bin/python\nimport os\nos.system('python /home/%s/Desktop/monitoring/monitor.py')" > /etc/init.d/uptmonitor''' % username)
            sudo('chmod 777 /etc/init.d/uptmonitor')
            
            with settings(warn_only=True):
                sudo('update-rc.d -f uptmonitor remove')
            sudo('update-rc.d uptmonitor defaults')
            
#            sudo('chmod 777 /home/%s/Desktop/monitoring/rest_service.py' % username)
            
#            with settings(warn_only=True):
#                sudo('chmod 777 /home/%s/Desktop/monitoring/performance.db' % username)
#                sudo('unlink /etc/init.d/uptmon')
#                sudo('unlink /etc/init.d/rest_service.py')
#                sudo('unlink /etc/init.d/performance.db')    

#            sudo('cp -rf /home/%s/Desktop/monitoring/monitor.py /etc/init.d/uptmon' % username)
#            sudo('cp -rf /home/%s/Desktop/monitoring/rest_service.py /etc/init.d/rest_service.py' % username)
#            with settings(warn_only=True):
#                sudo('cp -rf /home/%s/Desktop/monitoring/performance.db /etc/init.d/performance.db' % username)
#            sudo('chmod 777 /etc/init.d/rest_service.py')
#            with settings(warn_only=True):
#                sudo('chmod 777 /etc/init.d/performance.db')

#            with settings(warn_only=True):
#                sudo('update-rc.d -f uptmon remove')
#            sudo('update-rc.d uptmon defaults')
            
    except Exception, err:
        print err
#    put('%s/id.txt' %current_directory, '/home/%s/Desktop/slurp' %username)
#    put('%s/pymongo-1.11.tar.gz' %current_directory, '/home/%s/Desktop/slurp' %username)
#    put('%s/slurpservice.py' %current_directory, '/home/%s/Desktop/slurp' %username)
#    run('sudo cp -rf /home/%s/Desktop/slurp/slurpservice.py /usr/bin/slurpservice' %username)
#    run('sudo cp -rf /home/%s/Desktop/slurp/id.txt /usr/bin/' %username)
#    run('sudo chmod 755 /usr/bin/slurpservice')
#    run('sudo ln -s /usr/bin/slurpservice /etc/init.d/slurpservice')
#    run('sudo update-rc.d -f slurpservice remove')
#    run('sudo update-rc.d slurpservice defaults')
    
#def install():
#    with cd('/home/%s/Desktop/slurp/' %username):  
#        run('sudo dpkg -i python-pcapy_0.10.5-2ubuntu2_i386.deb')
#    with cd('/home/%s/pymongo-1.11/' %username):
#        sudo('python setup.py install')
#def slurp():    
#    with cd('/home/%s/Desktop/slurp/' %username):
#        sudo('/usr/bin/slurpservice > /dev/null', pty=False)
          
def running():
    copy()
#    install()
#    slurp()

