#!/usr/bin/python

from stompy import Stomp
from datetime import datetime, timedelta
from subprocess import Popen

import multiprocessing
import signal
import sys
import re
import commands
import json
import sched
import time
import urllib2
import pymongo
import smtplib

bw_db = pymongo.Connection().bams.bw

def signal_timeout(signum, frame):
    raise Exception

def schedule_monitor():
    stomp = Stomp(hostname='localhost', port=54123)
    stomp.connect()
    stomp.subscribe({'destination':'/monitor',
                     'ack':'client'})

    signal.signal(signal.SIGALRM, signal_timeout)
    try:
        up_down_data = {'timestamp': '%s-%s-%s-%s-%s' % (datetime.now().year,
                                                            datetime.now().month,
                                                            datetime.now().day,
                                                            datetime.now().hour,
                                                            datetime.now().minute), 'up': 0, 'down': 0}
        
        get_data = bw_db.find_one({'timestamp': datetime(datetime.now().year, datetime.now().month, datetime.now().day, datetime.now().hour, datetime.now().minute)})
        if get_data:
            stamping = '%s-%s-%s-%s-%s' %(get_data['timestamp'].year, get_data['timestamp'].month, get_data['timestamp'].day, get_data['timestamp'].hour, get_data['timestamp'].minute)
            get_data.update({'timestamp':stamping})
            get_data.pop('_id')
#            up_down_data = {'timestamp': '%s-%s-%s-%s-%s' %(get_data['timestamp'].year, get_data['timestamp'].month, get_data['timestamp'].day, get_data['timestamp'].hour, get_data['timestamp'].minute), 'up': get_data['up'], 'down': get_data['down']}
            
            
            signal.alarm(1)
            print 'sending data to template: %s' % get_data
            stomp.send({'destination': '/monitor',
                        'body': json.dumps(get_data),
                        'persistent': 'true'})
#        stomp.put(data, destination='/monitor')
            signal.alarm(0)
    except Exception, err:
        print err
        stomp.disconnect()

def go():
    while True:
        try:
            schedule_monitor()
        except Exception, err:
            print err
        time.sleep(60)
#    for i in range(60*24):
#        s.enter(60*i, 1, schedule_monitor, ())
#    s.run()
    
if __name__ == '__main__':    
    go()
