#!/usr/bin/python

import web
import bsddb
import json
import re

import sys, os, time, atexit
from signal import SIGTERM 

from datetime import datetime, timedelta

urls = (
    '/(.*)', 'monitor'
)
app = web.application(urls, globals())

class Daemon:
	"""
	A generic daemon class.
	
	Usage: subclass the Daemon class and override the run() method
	"""
	def __init__(self, pidfile, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
		self.stdin = stdin
		self.stdout = stdout
		self.stderr = stderr
		self.pidfile = pidfile
	
	def daemonize(self):
		"""
		do the UNIX double-fork magic, see Stevens' "Advanced 
		Programming in the UNIX Environment" for details (ISBN 0201563177)
		http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
		"""
		try: 
			pid = os.fork() 
			if pid > 0:
				# exit first parent
				sys.exit(0) 
		except OSError, e: 
			sys.stderr.write("fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
			sys.exit(1)
	
		# decouple from parent environment
		os.chdir("/") 
		os.setsid() 
		os.umask(0) 
	
		# do second fork
		try: 
			pid = os.fork() 
			if pid > 0:
				# exit from second parent
				sys.exit(0) 
		except OSError, e: 
			sys.stderr.write("fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
			sys.exit(1) 
	
		# redirect standard file descriptors
		sys.stdout.flush()
		sys.stderr.flush()
		si = file(self.stdin, 'r')
		so = file(self.stdout, 'a+')
		se = file(self.stderr, 'a+', 0)
		os.dup2(si.fileno(), sys.stdin.fileno())
		os.dup2(so.fileno(), sys.stdout.fileno())
		os.dup2(se.fileno(), sys.stderr.fileno())
	
		# write pidfile
		atexit.register(self.delpid)
		pid = str(os.getpid())
		file(self.pidfile,'w+').write("%s\n" % pid)
	
	def delpid(self):
		os.remove(self.pidfile)

	def start(self):
		"""
		Start the daemon
		"""
		# Check for a pidfile to see if the daemon already runs
		try:
			pf = file(self.pidfile,'r')
			pid = int(pf.read().strip())
			pf.close()
		except IOError:
			pid = None
			
		# Start the daemon
		self.daemonize()
		self.run()

	def stop(self):
		"""
		Stop the daemon
		"""
		# Get the pid from the pidfile
		try:
			pf = file(self.pidfile,'r')
			pid = int(pf.read().strip())
			pf.close()
		except IOError:
			pid = None
	
		if not pid:
			message = "pidfile %s does not exist. Daemon not running?\n"
			sys.stderr.write(message % self.pidfile)
			return # not an error in a restart

		# Try killing the daemon process	
		try:
			while 1:
				os.kill(pid, SIGTERM)
				time.sleep(0.1)
		except OSError, err:
			err = str(err)
			if err.find("No such process") > 0:
				if os.path.exists(self.pidfile):
					os.remove(self.pidfile)
			else:
				print str(err)
				sys.exit(1)

	def restart(self):
		"""
		Restart the daemon
		"""
		self.stop()
		self.start()

	def run(self):
		"""
		You should override this method when you subclass Daemon. It will be called after the process has been
		daemonized by start() or restart().
		"""

class WebService(Daemon):
        def run(self):
            app.run()

class monitor:        
    def GET(self, name):
        CUR_DIR = os.path.abspath(os.path.dirname(__file__))
        perform = bsddb.btopen(os.path.join(CUR_DIR,'performance.db'), 'c')
        #os.system('chmod 777'+os.path.join(CUR_DIR,'performance.db'))
        if name == 'get_data':    
            result = []
            for k, v in perform.iteritems():
                data = json.loads(v)
                complete_data = {k: data}
                result.append(complete_data)
            return json.dumps(result)
        if re.search(r'^get_data/up/[\d]+', name):
            result = []
            port = name.replace('get_data/up/', '')
            dump = bsddb.btopen(os.path.join(CUR_DIR,'storage/up_tcp_%s_dump.db' % port), 'c')
            for k, v in dump.iteritems():
                data = json.loads(v)
                complete_data = {k: data}
                result.append(complete_data)
            return json.dumps(result)
        if re.search(r'^get_data/down/[\d]+', name):
            result = []
            port = name.replace('get_data/down/', '')
            dump = bsddb.btopen(os.path.join(CUR_DIR,'storage/down_tcp_%s_dump.db' % port), 'c')
            for k, v in dump.iteritems():
                data = json.loads(v)
                complete_data = {k: data}
                result.append(complete_data)
            return json.dumps(result)

        if name == 'remove_data':
            os.system('rm -rf storage/*')
            return CUR_DIR
        if name == 'destroy':
            rest_pid = open(os.path.join(CUR_DIR, 'rest.pid'), 'r').read()
            monitor_pid = open('/tmp/monitor.pid', 'r').read()
            try:
                os.kill(int(monitor_pid),9)
            except Exception:
                pass
            os.system('rm -rf %s' % CUR_DIR)
            try:
                os.kill(int(rest_pid),9)
            except Exception:
                pass
            #os.system('update-rc.d -f monitor.py remove')

if __name__ == "__main__":
    CUR_DIR = os.path.abspath(os.path.dirname(__file__))
    service = WebService(os.path.join(CUR_DIR,'rest.pid'))
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            if 'start' == sys.argv[1]:
                sys.argv[1] = '0.0.0.0:1212'
                service.start()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)
