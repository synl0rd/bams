#!/usr/bin/python

# Bandiwidth Anomaly Monitoring System
# pattern case attack: Ping Flooding
# Prevention: Reject package from attacker

import pcapy
import re
import binascii
import os
import subprocess
import sys
import json
import time
import commands
import bsddb
import smtplib

from datetime import datetime
from multiprocessing import Process, Lock
import pymongo

from subprocess import Popen
from pymongo import DESCENDING, ASCENDING
from datetime import datetime, timedelta

settings_db = pymongo.Connection().bams.settings

ADMIN_GMAIL_USER = 'uptitenas@gmail.com'
ADMIN_GMAIL_PASS = '1515sukses'

def send_mail(message, to):
    try:
        email_acc = ADMIN_GMAIL_USER
        email_pwd = ADMIN_GMAIL_PASS
        smtpserver = smtplib.SMTP("smtp.gmail.com",587)
        print 'connecting to smtp.gmail.com:587'
        smtpserver.ehlo()
        smtpserver.starttls()
        smtpserver.ehlo
        print 'ehlo smtp.gmail.com:587'
        smtpserver.login(email_acc, email_pwd)
        print 'logged in to smtp.gmail.com:587'
        header = 'To:' + to + '\n' + 'From: BAMS <'+email_acc+'>\n' + 'Subject: ALERT!!\n'
        msg = header + '\n'+message+'\n\n'
        smtpserver.sendmail(email_acc, to, msg)
        print 'sending mail to: %s' % to
        smtpserver.close()
    except Exception, err:
        print 'failed send email: %s' % err
        return False
    else:
        print 'sending mail successful'
        return True

def send_sms(message, to):
#    os.system('python sms_sender.py "%s" %s' %(message,to))
    cur_path = os.path.abspath(os.path.dirname(__file__))
    Popen(['python',os.path.join(cur_path, 'sms_sender.py'),'"%s"' % message, to], close_fds=True)
    return True

class VoidSniff:
    # constructor untuk variabel class
    def __init__(self, pcap_filter):
         self.device = "any"  # network device yg akan di capture (set any untuk semua device)
         self.snaplen = 2048  # besar paket dalam 1 iterasi capture
         self.promisc = 1     # promiscious mode set pada network device yg di set
         self.to_ms = 100     # timeout per iterasi paket capture
         self.pcap_filter = pcap_filter # filter packet, berkeley packet filter format
         self.max_pkts = -1   # berpa banyak iterasi capturing, set -1 untuk tak hingga
         self.total_byte_down = 0 # define awal besar paket downstream
         self.total_byte_up = 0 # define awal besar paket upstream
         self.p = pcapy.open_live(self.device, self.snaplen, self.promisc, self.to_ms) # open capturing berdasarkan variabel yg di set
         
         # variabel penampung downstream dan upstream dengan struktur dict
         self.dump_up = {}
         self.dump_down = {}
         self.down = {}
         self.up = {}
         self.delta = datetime.now()
         
    def packethandler(self, hdr, data):
        byte = len(data) # hitung lebar data
        timestamp = datetime.now()
        contain = binascii.b2a_hex(data) # konversi binary text ke hexa ascii
        src_ip = '%s.%s.%s.%s' %(int('0x'+contain[56:58], 0),int('0x'+contain[58:60], 0),int('0x'+contain[60:62], 0),int('0x'+contain[62:64], 0)) # source ip
        dst_ip = '%s.%s.%s.%s' %(int('0x'+contain[64:66], 0),int('0x'+contain[66:68], 0),int('0x'+contain[68:70], 0),int('0x'+contain[70:72], 0)) # destination ip
        src_port = str(int('0x'+contain[72:76], 0)) # source port
        dst_port = str(int('0x'+contain[76:80], 0)) # destination port
         
        timestamp = '%s-%s-%s-%s-%s-%s-%s' % (datetime.now().year,
                                               datetime.now().month,
                                               datetime.now().day, 
                                               datetime.now().hour, 
                                               datetime.now().minute,
                                               datetime.now().second,
                                               datetime.now().microsecond
                                               )
         
         # PING Flooding Detection
         # the formula is 
         # flood = num_of_packet/sec >= 50 
        
        # deteksi ping flooding
        if self.pcap_filter == 'icmp':
             # define variabel penampung untuk IP dan waktu
             data = {'ip': src_ip,
                     'timestamp': timestamp,
             }
             # dump object data ke json string
             data = json.dumps(data)
             # create berkeley database file
             dumper = bsddb.btopen(os.path.join(PROJECT_PATH,'storage/icmp_dump.db'), 'c')
             # insert data dengan key timestamp ke berkeley db file
             dumper[timestamp] = data
             
             # check blacklist IP apakah sudah ada atau belum
             try:
                 blacklist = open('blacklist.json', 'r').read()
             except Exception, err:
                 blacklist = []
             else:
                 blacklist = json.loads(blacklist)
             
             # kondisi untuk flooding ping pattern
             if len(dumper) >= 50 and {'ip': src_ip} not in blacklist:
                 first = dumper.first()[0] # ambil data paket awal
                 # hitung selisih waktu paket baru dengan paket sebelumnya
                 delta = datetime.now() - datetime(int(first.split('-')[0]),
                                                          int(first.split('-')[1]),
                                                          int(first.split('-')[2]),
                                                          int(first.split('-')[3]),
                                                          int(first.split('-')[4]),
                                                          int(first.split('-')[5]),
                                                          int(first.split('-')[6]))
                 # jika selish paket dengan sebelumnya 0 detik
                 if delta.seconds == 0:
                     print '[!] ALERT! PING FLOODING FROM: %s' % src_ip
                     b_data = json.dumps([{'ip':src_ip}])

                     if settings_db.find_one({'username':'admin'}):
                         try:
                             to = settings_db.find_one({'username':'admin'})['email']
                             to_sms = settings_db.find_one({'username':'admin'})['hp']
                             mail_sent = send_mail('Alert! Ping Flooding Detected', to)
                             sms_sent = send_sms('Alert! Ping Flooding Detected', to_sms)
                         except Exception:
                             pass

                     # check data blacklist sebelumnya
                     try:
                         b_data_prev = open('blacklist.json', 'r').read()
                     except Exception:
                         pass
                     else:
                         # jika ada jumlahkan data baru dengan data sebelumnya kemudian tulis ulang
                         b_data = json.loads(b_data_prev) + json.loads(b_data)
                         b_data = json.dumps(b_data)
                     open('blacklist.json', 'w').write(b_data) # penulisan ulang data terbaru setelah di tambah ke file blacklist json (replace)
                     
                     # eksekusi blocking IP dengan iptables
                     os.system('iptables -A FORWARD -s %s -p icmp -j DROP' % src_ip)
                     os.system('iptables -A OUTPUT -s %s -p icmp -j DROP' % src_ip)
                     
                     print '[!] IP %s:%s Blocked!: dest %s:%s' % (src_ip, src_port, dst_ip, dst_port)
                 os.remove('storage/icmp_dump.db') # remove data icmp setelah proses chcking ping flooding
        else:            
            # filter jika port merupakan transaksi data ke mongodb tidak dihitung (akan menyebabkan infinite data jika tidak difilter)
            if src_port == '27017' or dst_port == '27017' or dst_port == '28017' or src_port == '28017' or src_ip == '0.0.0.0' or dst_ip == '0.0.0.0':
                pass
            else:
            # pengkondisian untuk upstream menggunakan regex & well known port
                if int(dst_port) < 49151 and re.search(r'192.168.', src_ip) or re.search(r'10.', src_ip) or re.search(r'172.[1-3]{1}[0-9]{1}.', src_ip):
                    times = '-'.join(timestamp.split('-')[:-2]) # set agar timestamp masuk per menit (slicing argumen datetime hingga menit)
                    self.total_byte_up += byte

                    # dynamic check untuk keperluan penghitungan bandiwdth terbesar port
                    if 'up%s' % dst_port not in self.up.keys():
                        self.up.update({'up%s' % dst_port:byte}) # update data upstream jika port sama dengan sebelumnya
                    else:
                        new_up = self.up['up%s' % dst_port] + byte 
                        self.up.update({'up%s' % dst_port:new_up}) # insert data upstream baru

                    if 'down%s' % src_port not in self.down.keys():
                        self.down.update({'down%s' % src_port:byte}) # update data downstream jika port sama dengan sebelumnya
                    else:
                        new_down = self.down['down%s' % src_port] + byte
                        self.down.update({'down%s' % src_port:new_down}) # insert data downstream baru

                    # checking per menit apakah waktu yg baru sudah ada di variabel penampung, jika tidak jalankan proses dibawah
                    if times not in self.dump_up.keys():
                        
                        # kalkulasi bandwidth upstream untuk semua port ke variabel penampung
                        bp_up = self.total_byte_up/60 # total bandwidth per menit
                        addition = {}
                        for x in self.up.keys():
                            up_kbps = self.up[x]/60/1000.00 # total byte bandwidth per menit dibagi 1000 agar KB/sec
                            addition.update({x:up_kbps})
                        for x in self.down.keys():
                            down_kbps = self.down[x]/60/1000.00
                            addition.update({x:down_kbps})
                        
                        #print addition
                        
                        # kalkulasi bandwidth upstream untuk semua port ke database mongodb
                        now_time = datetime(datetime.now().year,datetime.now().month,datetime.now().day,datetime.now().hour,datetime.now().minute)
                        self.dump_up = {times: bp_up/1000.00}
                        data = {'timestamp': now_time, 'up': bp_up/1000.00, 'down': 0, 'ip':src_ip}
                        data.update(addition)
                        if bw_db.find_one({'timestamp': now_time}):
                            x = {'up': bp_up/1000.00}
                            x.update(addition)
                            bw_db.update({'timestamp': now_time}, {'$set': x})
                        else:
                            bw_db.insert(data)
                        self.total_byte_up = 0
                        bp_up = 0
                        self.up = {}
                        #print 'UP: %s dst_port: %s, src_port: %s' % (self.dump_up, dst_port, src_port)
                
                # untuk downstream (proses sama dengan upstream
                else:
                    times = '-'.join(timestamp.split('-')[:-2])
                    self.total_byte_down += byte

                    if 'down%s' % src_port not in self.down.keys():
                        self.down.update({'down%s' % src_port:byte})
                    else:
                        new_down = self.down['down%s' % src_port] + byte
                        self.down.update({'down%s' % src_port:new_down})

                    if 'up%s' % dst_port not in self.up.keys():
                        self.up.update({'up%s' % dst_port:byte})
                    else:
                        new_up = self.up['up%s' % dst_port] + byte
                        self.up.update({'up%s' % dst_port:new_up})


                    if times not in self.dump_down.keys():
                        bp_down = self.total_byte_down/60
                        
                        addition = {}
                        for x in self.down.keys():
                            down_kbps = self.down[x]/60/1000.00
                            addition.update({x:down_kbps})
                        for x in self.up.keys():
                            up_kbps = self.up[x]/60/1000.00
                            addition.update({x:up_kbps})
                        
                        #print addition
                        now_time = datetime(datetime.now().year,datetime.now().month,datetime.now().day,datetime.now().hour,datetime.now().minute)
                        self.dump_down = {times: bp_down/1000.00}
                        data = {'timestamp': now_time, 'down': bp_down/1000.00, 'up': 0, 'ip':dst_ip}
                        data.update(addition)
                        if bw_db.find_one({'timestamp': now_time}):
                            x = {'down': bp_down/1000.00}
                            x.update(addition)
                            bw_db.update({'timestamp': now_time}, {'$set': x})
                        else:
                            bw_db.insert(data)
                        self.total_byte_down = 0
                        bp_down = 0
                        self.down = {}
                        #print 'DOWN: %s dst_port: %s, src_port: %s' % (self.dump_down, dst_port, src_port)
    # function untuk menjalankan proses capture (packet handler)
    def run(self):
         print '[+] Thread %s start' % self.pcap_filter
         self.p.setfilter(self.pcap_filter)
         self.p.loop(self.max_pkts, self.packethandler)

if __name__ == '__main__':
# daemonize mode
#    try:
#        ch = os.fork()
#        if ch > 0:
#            print 'Daemon PID: %d' %ch
#            sys.exit(0)
#    except OSError, e:
#        sys.exit(1)
    lock = Lock() # locking process
    PROJECT_PATH = os.path.abspath(os.path.dirname(__file__)) # dapatkan currently path absulute
    
    # checking direktori storage, jika tidak ada buat jika ada biarkan
    try:
        os.mkdir(os.path.join(PROJECT_PATH,'storage'))
    except Exception:
        pass
    
    bw_db = pymongo.Connection().bams.bw # set collection database
    
    gateway_ip = commands.getoutput('route | grep default') # check ip gateway
    try:
        protect_ip.append(re.search(r'default[ ]+([0-9\.]+) ', gateway_ip).groups()[0]) # gateway IP diprotek agar tidak ikut terbloking ping flooding
    except Exception, err:
        pass
    tcp_sniff = VoidSniff('tcp') # instasiasi class utk tcp handler
    icmp_sniff = VoidSniff('icmp') # instasiasi class utk icmp handler
    tcp_run = Process(target=tcp_sniff.run) # define proses untuk multiprocessing thread 1
    icmp_run = Process(target=icmp_sniff.run) # define proses untuk multiprocessing thread 2
    
    rest_run = subprocess.Popen(['python', os.path.join(PROJECT_PATH, 'rest_service.py'), 'start']) # eksekusi restful service ketika script ini dijalankan
    pid_rest = rest_run.pid # ambil PID dari rest service
    
    tcp_run.start() # jalankan thread 1
    icmp_run.start() # jalankan thread 2
    open(os.path.join(PROJECT_PATH, 'pid.txt'), 'w').write('%s:%s:%s' %(tcp_run.pid, icmp_run.pid, pid_rest)) # tulis PID rest service ke file
