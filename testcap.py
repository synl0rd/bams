#!/usr/bin/python

# Packet Capture Tester

import pcapy
import binascii
from datetime import datetime

class VoidSniff:
    # constructor untuk variabel class
    def __init__(self, pcap_filter=''):
         self.device = "any"  # network device yg akan di capture (set any untuk semua device)
         self.snaplen = 2048  # besar paket dalam 1 iterasi capture
         self.promisc = 1     # promiscious mode set pada network device yg di set
         self.to_ms = 100     # timeout per iterasi paket capture
         self.pcap_filter = pcap_filter # filter packet, berkeley packet filter format
         self.max_pkts = -1   # berpa banyak iterasi capturing, set -1 untuk tak hingga
         self.p = pcapy.open_live(self.device, self.snaplen, self.promisc, self.to_ms) # open capturing berdasarkan variabel yg di set
                  
    def packethandler(self, hdr, data):
        byte = len(data) # hitung lebar data
        timestamp = datetime.now()
        print '%s: %s Byte' % (timestamp, byte)
#        contain = binascii.b2a_hex(data) # konversi binary text ke hexa ascii
    # function untuk menjalankan proses capture (packet handler)

    def run(self):
         self.p.setfilter(self.pcap_filter)
         self.p.loop(self.max_pkts, self.packethandler)

if __name__ == '__main__':
    sniff = VoidSniff()
    sniff.run()
