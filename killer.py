import os

PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
pids = open(os.path.join(PROJECT_PATH, 'pid.txt'), 'r').read()
#rest_pid = open(os.path.join(PROJECT_PATH, 'rest.pid'), 'r').read()
pid1 = int(pids.split(':')[0])
pid2 = int(pids.split(':')[1])
pid3 = int(pids.split(':')[2])

try:
    print 'Killing PID1: %s' % pid1
    os.kill(pid1, 9)
except Exception, err:
    print err
else:
    print 'PID1 Killed'

try:
    print 'Killing PID2: %s' % pid2
    os.kill(pid2, 9)
except Exception, err:
    print err
else:
    print 'PID2 Killed'

try:
    print 'Killing PID3: %s' % pid3
    os.kill(pid3, 9)
except Exception, err:
    print err
else:
    print 'PID3 Killed'

#try:
#    print 'Killing Rest PID: %s' % rest_pid
#    os.kill(int(rest_pid), 9)
#except Exception, err:
#    print err
#else:
#    print 'Rest PID Killed'
