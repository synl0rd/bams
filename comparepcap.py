#!/usr/bin/python

# Packet Capture Tester

import pcapy
import sys
import binascii
import re
from datetime import datetime

class VoidSniff:
    # constructor untuk variabel class
    def __init__(self, pcap_filter=''):
         self.device = "any"  # network device yg akan di capture (set any untuk semua device)
         self.snaplen = 2048  # besar paket dalam 1 iterasi capture
         self.promisc = 1     # promiscious mode set pada network device yg di set
         self.to_ms = 100     # timeout per iterasi paket capture
         self.pcap_filter = pcap_filter # filter packet, berkeley packet filter format
         self.max_pkts = -1   # berpa banyak iterasi capturing, set -1 untuk tak hingga
         self.p = pcapy.open_live(self.device, self.snaplen, self.promisc, self.to_ms) # open capturing berdasarkan variabel yg di set
         self.total_bw_up = 0
         self.total_bw_down = 0
                  
    def packethandler(self, hdr, data):
        byte = len(data) # hitung lebar data
        timestamp = datetime.now()
        contain = binascii.b2a_hex(data) # konversi binary text ke hexa ascii
        src_ip = '%s.%s.%s.%s' %(int('0x'+contain[56:58], 0),int('0x'+contain[58:60], 0),int('0x'+contain[60:62], 0),int('0x'+contain[62:64], 0)) # source ip
        dst_ip = '%s.%s.%s.%s' %(int('0x'+contain[64:66], 0),int('0x'+contain[66:68], 0),int('0x'+contain[68:70], 0),int('0x'+contain[70:72], 0)) # destination ip
        src_port = str(int('0x'+contain[72:76], 0)) # source port
        dst_port = str(int('0x'+contain[76:80], 0)) # destination port

        if int(dst_port) < 1024:        
            self.total_bw_up += byte
        else:
            self.total_bw_down += byte
        print '%s: %s Byte Upstream, %s Byte Downstream\r' % (timestamp, 110349023, 150335021),
        sys.stdout.flush()

    def run(self):
         self.p.setfilter(self.pcap_filter)
         self.p.loop(self.max_pkts, self.packethandler)

if __name__ == '__main__':
    sniff = VoidSniff()
    sniff.run()
