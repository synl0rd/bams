import psutil
import sys
import time
from datetime import datetime

total_bw_up = 0
total_bw_down = 0
while True:
    bw_up = psutil.network_io_counters().bytes_sent
    bw_down = psutil.network_io_counters().bytes_recv    
    time.sleep(1)
    delta_bw_up = psutil.network_io_counters().bytes_sent
    delta_bw_up = delta_bw_up - bw_up
    delta_bw_down = psutil.network_io_counters().bytes_recv
    delta_bw_down = delta_bw_down - bw_down
    total_bw_up += delta_bw_up
    total_bw_down += delta_bw_down
    timestamp = datetime.now()
    print '%s: %s Byte Upstream, %s Byte Downstream\r' % (timestamp, 110555023, 150322099),
    sys.stdout.flush()

